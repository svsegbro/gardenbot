# Projects

GardenBot projects which may receive further attention one day:
* Scarecrobot: 
  * Gesture-making based on bird detection.

# Inspiration

* https://www.instructables.com/id/Arduino-Based-Humanoid-Robot-Using-Servo-Motors/
* Iron man mask:
  * https://www.youtube.com/watch?v=HBRoMPvzTcQ&list=PLsKebnYLuVOX5iKPndEBlYcSSca52h9Pk&index=3&t=0s
  * Pulley on servo
* Motors: 
  * https://www.hackster.io/taifur/complete-motor-guide-for-robotics-05d998  
* Homebridge:
  * https://www.npmjs.com/package/homebridge-weather-plus
  * https://homebridge.io/
  * http://localhost:8581
  * http://192.168.0.142:8581


# Hardware

* Multiple PWM's
  * https://www.adafruit.com/product/815


